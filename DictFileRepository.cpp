/*
 * DictFileRepository.cpp
 *
 *  Created on: 2017-01-13
 *      Author: etudiant
 *
 */
#include "DictFileRepository.h"
#include "my_utils.h"
DictFileRepository::DictFileRepository(std::string file_name) {
	using std::ifstream;
	using std::getline;
	assert(file_accessible(file_name));
	ifstream infile { file_name };
	std::string word;
	unsigned int word_max_size { 0 };

	unsigned int word_min_size { std::numeric_limits<unsigned int>::max() };
	set_type temp_check_data;

	while (getline(infile >> std::ws, word)) {
		assert(word != "\n" and word != " " and word != "");
		assert(word[0] != ' ');
		// todo: if assert fails, programming error in this method
		if (this->gen_data.find(word.length()) == this->gen_data.end()) {
			this->gen_data.insert(map_type::value_type(word.length(), std::vector<std::string> { word }));
		} else {
			this->gen_data.at(word.length()).push_back(word);
		}
		if (word.length() > word_max_size) {
			word_max_size = word.length();
		}
		if (word.length() < word_min_size) {
			word_min_size = word.length();
		}

		temp_check_data.insert(word);
	}
	this->max_word_size = word_max_size;
	this->min_word_size = word_min_size;
	this->ptr_check_data = std::make_shared<const set_type>(temp_check_data);
}

