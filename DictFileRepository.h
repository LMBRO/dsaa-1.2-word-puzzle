/*
 * DictFileRepository.h
 *
 *  Created on: 2017-01-13
 *      Author: etudiant
 */

#ifndef DICTFILEREPOSITORY_H_
#define DICTFILEREPOSITORY_H_
#include "my_utils.h"
#include "DictRepository.h"
class DictFileRepository: public DictRepository {
public:
	DictFileRepository(std::string);
};

#endif /* DICTFILEREPOSITORY_H_ */
