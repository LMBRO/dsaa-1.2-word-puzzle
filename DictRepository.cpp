/*
 * DictRepository.cpp
 *
 *  Created on: 2017-01-13
 *      Author: etudiant

 */
#include "DictRepository.h"

Puzzle DictRepository::generate_puzzle(std::vector<std::vector<char> > cases) {
	Puzzle p{ptr_check_data};
	p.cases = std::move(cases);
	p.max_word_size = max_word_size;
	p.min_word_size = min_word_size;
	return p;
}

//DictRepository::DictRepository() {
//}
