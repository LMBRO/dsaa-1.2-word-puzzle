/*
 * DictRepository.h
 *
 *  Created on: 2017-01-13
 *      Author: etudiant
 */

#ifndef DICTREPOSITORY_H_
#define DICTREPOSITORY_H_
#include <map>
#include <set>
#include <string>
#include <memory>
#include <fstream>
#include <vector>
#include <cassert>
#include <limits>

#include "Puzzle.h"
typedef std::map<int, std::vector<std::string>> map_type;
typedef std::set<std::string> set_type;
//enum Direction {N, NE, E, SE, S, SW, W, NW };
//struct PuzzleGenElements{
//	unsigned int x;
//	unsigned int y;
//	Direction dir;
//	std::string word;
//};
class DictRepository {
public:
	//todo: Multiple possible overloads for generate_puzzle

	//Deterministic
	Puzzle generate_puzzle(std::vector<std::vector<char>>);
	Puzzle generate_puzzle(unsigned int) = delete;
protected:
	DictRepository() = default;
	DictRepository(DictRepository &) = delete;

	map_type gen_data;
	std::shared_ptr<const set_type> ptr_check_data;
	unsigned int max_word_size;
	unsigned int min_word_size;
};
#endif /* DICTREPOSITORY_H_ */
