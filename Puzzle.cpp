#include "Puzzle.h"

Puzzle::Puzzle(std::shared_ptr<const set_type> pcheck) :
		ptr_check_data { std::move(pcheck) } {

}

Puzzle::~Puzzle() {
}
// todo: for now I'm simply rreturning a vector of found words. Eventually It could be interesting to return more e.g the position of the found words (return a struct ?)
std::set<std::string> Puzzle::solve() const {
	std::set<std::string> found_words { };
	size_t puz_size { cases.size() };
	//row is a copy
	for (std::vector<char> row : cases) {
		add_words_both_directions(found_words, row, puz_size);

	}
	std::cout << "ok rows\n" << std::endl;

//	bool stop { false };
//	bool reversed { false };
//	//get words in rows, both directions
//	while (!stop) {
//		for (std::vector<char> & row : cases) {
//			//search in single column, left to right
//			std::vector<std::string> words_in_row = find_words(row, puz_size);
//			for (auto const & w : words_in_row) {
//				found_words.push_back(w);
//			}
//			if (!reversed) {
//				std::reverse(row.begin(), row.end());
//				reversed = true;
//			} else {
//				stop = true;
//			}
//		}
//	}
	//get words in columns, both direction
	for (size_t col_index { 0 }; col_index != puz_size; col_index++) {
		std::vector<char> col;
		col.reserve(puz_size);
		for (size_t row_index { 0 }; row_index != puz_size; row_index++) {
			col.push_back(cases[row_index][col_index]);
		}
		//col avaible here
		add_words_both_directions(found_words, col, puz_size);
	}
	std::cout << "ok cols\n" << std::endl;
	//diagonals top left bottom right, top (size_puz)
	for (size_t col_index { 0 }; col_index != puz_size; col_index++) {
		size_t work_col_index { col_index };
		size_t work_row_index { 0 };
		std::vector<char> diag;
		diag.reserve(puz_size); //todo too much, there's probably a formula to use here same for below
		while (work_col_index != puz_size) {
			diag.push_back(cases[work_row_index][work_col_index]);
			work_row_index++;
			work_col_index++;
		}
		add_words_both_directions(found_words, diag, puz_size);
	}
	std::cout << "ok diags top left, top \n" << std::endl;
	//diagonals top left bottom right, bottom (size_puz - 1)
	for (size_t row_index { 1 }; row_index != puz_size; row_index++) {
		size_t work_col_index { 0 };
		size_t work_row_index { row_index };
		std::vector<char> diag;
		diag.reserve(puz_size - 1);
		while (work_row_index != puz_size) {
			diag.push_back(cases[work_row_index][work_col_index]);
			work_row_index++;
			work_col_index++;
		}
		add_words_both_directions(found_words, diag, puz_size);
	}
	std::cout << "ok diags top left, bottom \n" << std::endl;
	//diagonals bottom left top right, top (puz_size)
	for (size_t row_index = 0; row_index != puz_size;row_index++) {
		size_t work_col_index { 0 };
		size_t work_row_index { row_index };
		std::vector<char> diag;
		diag.reserve(puz_size);
		bool row_reached_zero { false };
		while (!row_reached_zero) {
			diag.push_back(cases[work_row_index][work_col_index]);
			if (work_row_index == 0) {
				row_reached_zero = true;
			} else {
				work_row_index--;
				work_col_index++;
			}
		}
		add_words_both_directions(found_words, diag, puz_size);
	}
	std::cout << "ok diags top right, top \n" << std::endl;
	//diagonals bottom left top right bottom (puz_size -1)
	for(size_t col_index{1};col_index != puz_size; col_index++){
		size_t work_col_index{col_index};
		size_t work_row_index{puz_size - 1};
		std::vector<char> diag;
		diag.reserve(puz_size - 1);
		while(work_col_index != puz_size){
			diag.push_back(cases[work_row_index][work_col_index]);
			work_row_index--;
			work_col_index++;
		}
	add_words_both_directions(found_words, diag, puz_size);
	}
	std::cout << "ok diags top right, bottom \n" << std::endl;
	return found_words;
}
void Puzzle::add_words_both_directions(std::set<std::string> & found_words, std::vector<char> & vec,
		size_t puz_size) const {
	std::vector<std::string> words_in_row = find_words(vec, puz_size);
	std::reverse(vec.begin(), vec.end());
	std::vector<std::string> words_in_reversed_row = find_words(vec, puz_size);
	for (auto const & w : words_in_row) {
		found_words.insert(w);
	}
	for (auto const & w : words_in_reversed_row) {
		found_words.insert(w);
	}
}
std::vector<std::string> Puzzle::find_words(std::vector<char> & vec, size_t puz_size) const {
	std::vector<std::string> found_words { };
	for (size_t findex = 0; findex != puz_size - min_word_size + 1; findex++) {

		for (size_t lindex = findex + min_word_size - 1; lindex != std::min(findex + max_word_size, puz_size);
				lindex++) {
			std::string str { vec.begin() + findex, vec.begin() + lindex + 1 };
			if (check_word(str)) {
				found_words.push_back(str);
			}

		}

	}
	return found_words;

}
std::ostream & operator<<(std::ostream & os, const Puzzle & puz) {
	for (std::vector<char> const & vec2 : puz.cases) {
		for (char c : vec2) {
			os << c;
			os << ' ';
			//todo: don't print a space for the last character in a row.1
		}
		os << '\n';

	}
}

bool Puzzle::check_word(std::string const & w) const {
//std::cout << w << " " << this->words_for_check.count(w) << " ";
	return ptr_check_data->count(w) == 1;
}

