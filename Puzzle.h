#ifndef PUZZLE_H_
#define PUZZLE_H_
#include <string>
#include <map>
#include <vector>
#include <set>
#include <fstream>
#include <iostream>
#include <memory>
#include <cassert>
#include <algorithm>

//#include "DictRepository.h"
typedef std::set<std::string> set_type;
class Puzzle {
	friend class DictRepository;
public:
	Puzzle(std::shared_ptr<const set_type>);
	~Puzzle();
	std::set<std::string> solve() const;
	friend std::ostream & operator<<(std::ostream &, const Puzzle &);
	bool check_word(std::string const & w) const;
private:
	std::vector<std::vector<char>> cases;
	std::shared_ptr<const set_type> ptr_check_data;
	unsigned int max_word_size;
	unsigned int min_word_size;

	std::vector<std::string> find_words(std::vector<char> &, size_t) const;
	void add_words_both_directions(std::set<std::string> &, std::vector<char> &, size_t) const;
};

#endif
