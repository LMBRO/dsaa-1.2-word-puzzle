/*
 * main.cpp
 *
 *  Created on: 2017-01-09
 *      Author: etudiant
 */
#include <iostream>

#include "Puzzle.h"
#include "DictFileRepository.h"
int main() {
	using std::cout;
	using std::endl;
	cout << "ok in function\n" << endl;
	std::vector<std::vector<char>> chars { {'b', 'a', 't'},
											{'z', 'i', 'g'},
											{'d', 'a', 't'}

													};
	cout << "ok chars constructed\n" << endl;
	DictFileRepository dict{"updated.txt"};
	cout << "ok dict constructed\n" << endl;
	Puzzle p = dict.generate_puzzle(chars);
	cout << "ok puzzle generated\n" << endl;
	std::set<std::string> word_list{p.solve()};
	cout << "ok solution (list of words) obtained\n" << endl;
	for(std::string const & word: word_list){
		std::cout << word << std::endl;
	}
//	cout << "HELLO WORLD!!!!" << endl;

	return 0;
}

