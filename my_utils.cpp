/*
 * my_utils.cpp
 *
 *  Created on: 2017-01-09
 *      Author: etudiant
 */
#include "my_utils.h"

bool file_accessible(std::string const & file_name) {
	std::ifstream infile { file_name };
	return infile.good();
}

std::ostream & print_solution(std::ostream & os, std::vector<std::vector<char>> vec1) {
	for (std::vector<char> const & vec2 : vec1) {
		for (char c: vec2){
			os << c;
			os << ' ';
		//todo: don't print a space for the last character in a row.1
		}
		os << '\n';
	}

}

