/*
 * my_utils.h
 *
 *  Created on: 2017-01-09
 *      Author: etudiant
 */
#ifndef MY_UTILS_H_
#define MY_UTILS_H_
#include <string>
#include <fstream>
#include <ostream>
#include <vector>
bool file_accessible(std::string const &);
std::ostream & print_solution(std::ostream &, std::vector<std::vector<char>> const &);
#endif /* MY_UTILS_H_ */
